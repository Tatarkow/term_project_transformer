# Transformer
This program transforms DSL for notes and vocals to the format used by Lilypond.

## How to run it
Execute
```
javac cz/tatarko/*.java
java cz.tatarko.Main ../data/input.txt ../data/output.txt
```
to read the sample input in our DSL, transform it and store the results into the output file.
