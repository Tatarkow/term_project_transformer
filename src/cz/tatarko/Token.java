package cz.tatarko;

public class Token {
    public String vocal = "";
    public NoteType type;
    public NoteShift shift;
    public int length;

    public String noteToString() {
        return NoteType.noteToString(type) + NoteShift.noteToString(shift) + length;
    }
}
