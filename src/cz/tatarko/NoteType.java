package cz.tatarko;

public enum NoteType {
    C,
    D,
    E,
    F,
    G,
    A,
    H,
    B;

    public static NoteType parse(String raw) throws SyntaxException {
        NoteType noteType;

        switch (raw) {
            case "c": noteType = C; break;
            case "d": noteType = D; break;
            case "e": noteType = E; break;
            case "f": noteType = F; break;
            case "g": noteType = G; break;
            case "a": noteType = A; break;
            case "h": noteType = H; break;
            case "b": noteType = B; break;
            default: throw new SyntaxException();
        }

        return noteType;
    }

    public static String noteToString(NoteType noteType) {
        switch (noteType) {
            case C: return "c";
            case D: return "d";
            case E: return "e";
            case F: return "f";
            case G: return "g";
            case A: return "a";
            case H: return "h";
            case B: return "b";
            default: return null;
        }
    }
}
