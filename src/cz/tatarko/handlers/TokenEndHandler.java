package cz.tatarko.handlers;

import cz.tatarko.*;

public class TokenEndHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;

        if ("}".equals(word)) {
            fullState.state = State.INIT;
        }
        else if (word.startsWith("\\")) {
            TokenOrPhrase tokenOrPhrase = new TokenOrPhrase();
            String tokenName = word.substring(1);
            tokenOrPhrase.phrase = fullState.phrases.get(tokenName);

            if (fullState.isSong) {
                fullState.song.tokenOrReferenceList.add(tokenOrPhrase);
            }
            else {
                fullState.lastPhrase.tokenOrReferenceList.add(tokenOrPhrase);
            }

            fullState.state = State.TOKEN_END;
        }
        else {
            NoteTypeAndShiftHandler.handle(fullState);
        }
    }
}
