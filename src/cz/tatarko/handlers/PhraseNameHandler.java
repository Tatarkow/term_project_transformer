package cz.tatarko.handlers;

import cz.tatarko.FullState;
import cz.tatarko.State;
import cz.tatarko.SyntaxException;

public class PhraseNameHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        if ("{".equals(fullState.word)) {
            fullState.state = State.TOKEN_END;
        }
        else {
            throw new SyntaxException();
        }
    }
}
