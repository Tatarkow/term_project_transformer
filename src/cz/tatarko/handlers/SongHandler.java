package cz.tatarko.handlers;

import cz.tatarko.*;

public class SongHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;

        if (word.contains("{") || word.contains("}")) {
            throw new SyntaxException();
        }

        fullState.song = new Song(word);
        fullState.isSong = true;
        fullState.state = State.NAME;
    }
}
