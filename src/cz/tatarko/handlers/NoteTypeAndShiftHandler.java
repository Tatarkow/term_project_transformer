package cz.tatarko.handlers;

import cz.tatarko.*;

public class NoteTypeAndShiftHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;
        String noteTypeRaw;
        String noteShiftRaw;

        if (word.contains("+")) {
            noteTypeRaw = word.split("\\+")[0];
            noteShiftRaw = "+" + word.split("\\+")[1];
        }
        else if (word.contains("-")) {
            noteTypeRaw = word.split("-")[0];
            noteShiftRaw = "-" + word.split("-")[1];
        }
        else {
            noteTypeRaw = word;
            noteShiftRaw = "0";
        }

        Token token = new Token();
        token.type = NoteType.parse(noteTypeRaw);
        token.shift = NoteShift.parse(noteShiftRaw);

        TokenOrPhrase tokenOrPhrase = new TokenOrPhrase();
        tokenOrPhrase.token = token;

        if (fullState.isSong) {
            fullState.song.tokenOrReferenceList.add(tokenOrPhrase);
        }
        else {
            fullState.lastPhrase.tokenOrReferenceList.add(tokenOrPhrase);
        }

        fullState.state = State.TOKEN_START;
    }
}
