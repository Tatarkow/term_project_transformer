package cz.tatarko.handlers;

import cz.tatarko.FullState;
import cz.tatarko.State;
import cz.tatarko.SyntaxException;
import cz.tatarko.TokenOrPhrase;

import java.util.List;

public class TokenStartHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        int length;

        try {
            length = Integer.parseInt(fullState.word);
        }
        catch (NumberFormatException e) {
            throw new SyntaxException();
        }

        if (length != 1 && length % 2 != 0) {
            throw new SyntaxException();
        }

        List<TokenOrPhrase> tokenOrReferenceList;

        if (fullState.isSong) {
            tokenOrReferenceList = fullState.song.tokenOrReferenceList;
        }
        else {
            tokenOrReferenceList = fullState.lastPhrase.tokenOrReferenceList;
        }

        TokenOrPhrase tokenOrPhrase = tokenOrReferenceList.get(tokenOrReferenceList.size() - 1);
        tokenOrPhrase.token.length = length;

        fullState.state = State.NOTE_LENGTH;
    }
}
