package cz.tatarko.handlers;

import cz.tatarko.FullState;
import cz.tatarko.State;
import cz.tatarko.SyntaxException;
import cz.tatarko.TokenOrPhrase;

import java.util.List;

public class NoteLengthHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;

        if ("}".equals(word)) {
            fullState.state = State.INIT;
        }
        else if (word.startsWith("\"") && word.endsWith("\"")) {
            String vocal = word.substring(1, word.length()-1);

            List<TokenOrPhrase> tokenOrReferenceList;

            if (fullState.isSong) {
                tokenOrReferenceList = fullState.song.tokenOrReferenceList;
            }
            else {
                tokenOrReferenceList = fullState.lastPhrase.tokenOrReferenceList;
            }

            TokenOrPhrase tokenOrReference = tokenOrReferenceList.get(tokenOrReferenceList.size() - 1);
            tokenOrReference.token.vocal = vocal;

            fullState.state = State.TOKEN_END;
        }
        else {
            NoteTypeAndShiftHandler.handle(fullState);
        }
    }
}
