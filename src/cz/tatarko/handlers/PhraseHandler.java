package cz.tatarko.handlers;

import cz.tatarko.FullState;
import cz.tatarko.Phrase;
import cz.tatarko.State;
import cz.tatarko.SyntaxException;

public class PhraseHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;

        if (word.contains("{") || word.contains("}")) {
            throw new SyntaxException();
        }

        fullState.lastPhrase = new Phrase(word);
        fullState.phrases.put(word, fullState.lastPhrase);
        fullState.isSong = false;
        fullState.state = State.NAME;
    }
}
