package cz.tatarko.handlers;

import cz.tatarko.FullState;
import cz.tatarko.State;
import cz.tatarko.SyntaxException;

public class InitHandler {
    public static void handle(FullState fullState) throws SyntaxException {
        String word = fullState.word;

        if ("phrase".equals(word)) {
            fullState.state = State.PHRASE;
        }
        else if ("song".equals(word)) {
            fullState.state = State.SONG;
        }
        else {
            throw new SyntaxException();
        }
    }
}
