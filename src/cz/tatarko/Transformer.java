package cz.tatarko;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import cz.tatarko.handlers.*;

public class Transformer {
    static void transform(String inputFilename, String outputFilename) {
        FullState fullState = new FullState();

        parse(fullState, inputFilename);

        try {
            print(fullState, outputFilename);
        } catch (IOException e) {
            System.err.println("There was a problem with the output file.");
        }
    }

    private static void print(FullState fullState, String outputFilename) throws IOException {
        FileWriter fileWriter = new FileWriter(outputFilename);

        fileWriter.write(
        "\\version \"2.18.2\"\n\n" +
            "\\header {\n" +
            "\ttitle = \"" + fullState.song.name + "\"\n}\n\n" +
            "\\score {\n" +
            "\t<<\n" +
            "\t\t\\new Staff {\n" +
            "\t\t\t\\key c \\major\n" +
            "\t\t\t\\time 4/4\n\n" +
            "\t\t\t\\absolute {\n\t\t\t\t"
        );

        printNotes(fullState.song.tokenOrReferenceList, fileWriter);

        fileWriter.write(
        "\n\t\t\t}\n" +
            "\t\t}\n\n" +
            "\t\t\\addlyrics {\n\t\t\t"
        );

        printVocals(fullState.song.tokenOrReferenceList, fileWriter);

        fileWriter.write(
            "\n\t\t}\n" +
            "\t>>\n" +
            "}"
        );

        fileWriter.close();
    }

    static private void printVocals(List<TokenOrPhrase> tokenOrPhraseList, FileWriter fileWriter) throws IOException {
        for (TokenOrPhrase tokenOrPhrase: tokenOrPhraseList) {
            if (tokenOrPhrase.token == null) {
                printVocals(tokenOrPhrase.phrase.tokenOrReferenceList, fileWriter);
            }
            else {
                fileWriter.write("\"" + tokenOrPhrase.token.vocal + "\" ");
            }
        }
    }

    static private void printNotes(List<TokenOrPhrase> tokenOrPhraseList, FileWriter fileWriter) throws IOException {
        for (TokenOrPhrase tokenOrPhrase: tokenOrPhraseList) {
            if (tokenOrPhrase.token == null) {
                printNotes(tokenOrPhrase.phrase.tokenOrReferenceList, fileWriter);
            }
            else {
                fileWriter.write(tokenOrPhrase.token.noteToString() + " ");
            }
        }
    }

    static private void parse(FullState fullState, String inputFilename) {
        List<String> words;

        try {
            words = getWords(inputFilename);
        } catch (FileNotFoundException e) {
            System.err.println("The input file does not exists.");
            return;
        }

        for (String word: words) {
            fullState.word = word;

            try {
                switch (fullState.state) {
                    case INIT: InitHandler.handle(fullState); break;
                    case PHRASE: PhraseHandler.handle(fullState); break;
                    case NAME: PhraseNameHandler.handle(fullState); break;
                    case TOKEN_END: TokenEndHandler.handle(fullState); break;
                    case TOKEN_START: TokenStartHandler.handle(fullState); break;
                    case NOTE_LENGTH: NoteLengthHandler.handle(fullState); break;
                    case SONG: SongHandler.handle(fullState); break;
                }
            } catch (SyntaxException e) {
                System.err.println("Format of the input is invalid.");
                return;
            }
        }
    }

    private static List<String> getWords(String inputFilename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(inputFilename));

        List<String> words = new ArrayList<>();

        while(scanner.hasNext()){
            String line = scanner.nextLine();

            words.addAll(Arrays
                .stream(line.replace("\"}", "\" }").split("\\s+"))
                .filter(word -> !word.equals(""))
                .collect(Collectors.toList())
            );
        }

        return words;
    }
}
