package cz.tatarko;

public enum NoteShift {
    MINUS_TWO,
    MINUS_ONE,
    BASE,
    PLUS_ONE,
    PLUS_TWO;

    public static NoteShift parse(String raw) throws SyntaxException {
        NoteShift noteShift;

        switch (raw) {
            case "-2": noteShift = NoteShift.MINUS_TWO; break;
            case "-1": noteShift = NoteShift.MINUS_ONE; break;
            case "0": noteShift = NoteShift.BASE; break;
            case "+1": noteShift = NoteShift.PLUS_ONE; break;
            case "+2": noteShift = NoteShift.PLUS_TWO; break;
            default: throw new SyntaxException();
        }

        return noteShift;
    }

    public static String noteToString(NoteShift noteShift) {
        switch (noteShift) {
            case MINUS_TWO: return ",,";
            case MINUS_ONE: return ",";
            case BASE: return "";
            case PLUS_ONE: return "'";
            case PLUS_TWO: return "''";
            default: return null;
        }
    }
}
