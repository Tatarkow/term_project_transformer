package cz.tatarko;

import java.util.ArrayList;
import java.util.List;

public class Song {
    public String name;
    public List<TokenOrPhrase> tokenOrReferenceList = new ArrayList<>();

    public Song(String name) {
        this.name = name;
    }
}