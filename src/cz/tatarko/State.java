package cz.tatarko;

public enum State {
    INIT,
    PHRASE,
    SONG,
    NAME,
    TOKEN_START,
    NOTE_LENGTH,
    TOKEN_END,
}
