package cz.tatarko;

public class Main {

    public static void main(String[] args) {
        if (args.length == 2) {
            Transformer.transform(args[0], args[1]);
        }
        else {
            System.err.println(
                "Exactly two arguments must be provided.\n" +
                "The first one if path & filename of the input.\n" +
                "The second one if path & filename of the output."
            );
        }
    }
}
