package cz.tatarko;

import java.util.HashMap;
import java.util.Map;

public class FullState {
    public String word = "";
    public State state = State.INIT;
    public Map<String, Phrase> phrases = new HashMap<>();
    public Phrase lastPhrase = null;
    public Song song = null;
    public boolean isSong = false;
}
