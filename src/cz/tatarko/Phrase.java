package cz.tatarko;

import java.util.ArrayList;
import java.util.List;

public class Phrase {
    public String name;
    public List<TokenOrPhrase> tokenOrReferenceList = new ArrayList<>();

    public Phrase(String name) {
        this.name = name;
    }
}
